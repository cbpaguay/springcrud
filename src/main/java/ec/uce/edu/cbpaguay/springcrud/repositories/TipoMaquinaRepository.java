package ec.uce.edu.cbpaguay.springcrud.repositories;

import ec.uce.edu.cbpaguay.springcrud.entities.TipoMaquina;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TipoMaquinaRepository extends JpaRepository<TipoMaquina,Long> {
}
