package ec.uce.edu.cbpaguay.springcrud.entities;

import javax.persistence.*;

@Entity
@Table(name = "maquina")
public class Maquina {
    @Id
    @GeneratedValue
    private Long idMaquina;

    private String nombre;

    @ManyToOne
    @JoinColumn(name = "idTipo")
    private TipoMaquina tipoMaquina;

    //get, set, constructor

    public Maquina() {
    }

    public Long getIdMaquina() {
        return idMaquina;
    }

    public void setIdMaquina(Long idMaquina) {
        this.idMaquina = idMaquina;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public TipoMaquina getTipoMaquina() {
        return tipoMaquina;
    }

    public void setTipoMaquina(TipoMaquina tipoMaquina) {
        this.tipoMaquina = tipoMaquina;
    }
}
