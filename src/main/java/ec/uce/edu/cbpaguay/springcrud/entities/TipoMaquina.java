package ec.uce.edu.cbpaguay.springcrud.entities;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "tipo_maquina")
public class TipoMaquina {
    @Id
    @GeneratedValue
    private Long idTipo;

    private String nombre;

    //@OneToMany(mappedBy = "idMaquina")
    //private Set<Maquina> maquinas = new HashSet<>();

    //set, get, constructor

    public TipoMaquina() {
    }

    public Long getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(Long idTipo) {
        this.idTipo = idTipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /*public Set<Maquina> getMaquinas() {
        return maquinas;
    }

    public void setMaquinas(Set<Maquina> maquinas) {
        this.maquinas = maquinas;
    }*/
}
