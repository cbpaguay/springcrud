package ec.uce.edu.cbpaguay.springcrud.controllers;

import ec.uce.edu.cbpaguay.springcrud.entities.TipoMaquina;
import ec.uce.edu.cbpaguay.springcrud.repositories.TipoMaquinaRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/tipomaquinas")
public class TipoMaquinaController {
    private final TipoMaquinaRepository tipoMaquinaRepository;

    public TipoMaquinaController(TipoMaquinaRepository tipoMaquinaRepository) {
        this.tipoMaquinaRepository = tipoMaquinaRepository;
    }

    @GetMapping
    public List<TipoMaquina> getTipoMaquinas(){
        return tipoMaquinaRepository.findAll();
    }

    @GetMapping("/{id}")
    public TipoMaquina getTipoMaquina(@PathVariable Long id){
        return tipoMaquinaRepository.findById(id).orElseThrow(RuntimeException::new);
    }

    @PostMapping
    public ResponseEntity createTipoMaquina(@RequestBody TipoMaquina tipoMaquina) throws URISyntaxException {
        TipoMaquina savedTipoMaquina = tipoMaquinaRepository.save(tipoMaquina);
        return ResponseEntity.created(new URI("/tipomaquinas/" + savedTipoMaquina.getIdTipo())).body(savedTipoMaquina);
    }

    @PutMapping("/{id}")
    public ResponseEntity updateTipoMaquina(@PathVariable Long id, @RequestBody TipoMaquina tipoMaquina){
        TipoMaquina currentTipoMaquina = tipoMaquinaRepository.findById(id).orElseThrow(RuntimeException::new);
        currentTipoMaquina.setNombre(tipoMaquina.getNombre());
        currentTipoMaquina = tipoMaquinaRepository.save(tipoMaquina);

        return ResponseEntity.ok(currentTipoMaquina);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteTipoMaquina(@PathVariable Long id){
        tipoMaquinaRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }
}
