package ec.uce.edu.cbpaguay.springcrud.repositories;

import ec.uce.edu.cbpaguay.springcrud.entities.Maquina;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MaquinaRepository extends JpaRepository<Maquina,Long> {
}
