package ec.uce.edu.cbpaguay.springcrud.controllers;

import ec.uce.edu.cbpaguay.springcrud.entities.Maquina;
import ec.uce.edu.cbpaguay.springcrud.repositories.MaquinaRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/maquinas")
public class MaquinaController {
    public final MaquinaRepository maquinaRepository;

    public MaquinaController(MaquinaRepository maquinaRepository) {
        this.maquinaRepository = maquinaRepository;
    }

    @GetMapping
    public List<Maquina> getMaquinas(){
        return maquinaRepository.findAll();
    }

    @GetMapping("/{id}")
    public Maquina getMaquina(@PathVariable Long id){
        return maquinaRepository.findById(id).orElseThrow(RuntimeException::new);
    }

    @PostMapping
    public ResponseEntity createMaquina(@RequestBody Maquina maquina) throws URISyntaxException {
        Maquina savedMaquina = maquinaRepository.save(maquina);
        return ResponseEntity.created(new URI("/maquinas/" + savedMaquina.getIdMaquina())).body(savedMaquina);
    }

    @PutMapping("/{id}")
    public ResponseEntity updateMaquina(@PathVariable Long id, @RequestBody Maquina maquina){
        Maquina currentMaquina = maquinaRepository.findById(id).orElseThrow(RuntimeException::new);
        currentMaquina.setNombre(maquina.getNombre());
        currentMaquina = maquinaRepository.save(maquina);

        return ResponseEntity.ok(currentMaquina);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteMaquina(@PathVariable Long id){
        maquinaRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }
}
