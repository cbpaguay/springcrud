import React, {Component} from 'react';
import {Navbar,Nav,NavDropdown} from "react-bootstrap";
import {Link} from "react-router-dom";

export default class NavigationBar extends Component {
    render() {

    return(
        <Navbar bg="dark" variant="dark">
            <Link to={""} className={"navbar-brand"}>
                CRUD
            </Link>
            <Nav className="mr-auto">
                <NavDropdown title="Tipo Máquinas" id="navbarScrollingDropdown">
                    <Link to={"tipomaquinas"} className={"dropdown-item"}>Listar</Link>
                    <Link to={"addtipo"} className={"dropdown-item"}>Añadir</Link>
                </NavDropdown>
                <NavDropdown title="Máquinas" id="navbarScrollingDropdown">
                    <Link to={"listmaquina"} className={"dropdown-item"}>Listar</Link>
                    <Link to={"addmaquina"} className={"dropdown-item"}>Añadir</Link>
                </NavDropdown>
            </Nav>
        </Navbar>
    );
};
};