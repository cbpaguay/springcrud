import React, {Component} from "react";
import { Link, withRouter } from 'react-router-dom';
import { Button, Container, Form, FormGroup, Input, Label } from 'reactstrap';
import NavigationBar from "./NavigationBar";

export default class TipoMaquinaEdit extends Component{
    emptyItem = {
        nombre: ''
    };

    constructor(props) {
        super(props);
        this.state = {
            item: this.emptyItem
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async componentDidMount() {
        if (this.props.match.params.id !== 'new') {
            const tipo = await (await fetch(`/tipomaquinas/${this.props.match.params.id}`)).json();
            this.setState({item: tipo});
        }
    }
    handleChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        let item = {...this.state.item};
        item[name] = value;
        this.setState({item});
    }
    async handleSubmit(event) {
        event.preventDefault();
        const {item} = this.state;

        await fetch('/tipomaquinas' + (item.idTipo ? '/' + item.idTipo : ''), {
            method: (item.idTipo) ? 'PUT' : 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(item),
        });
        this.props.history.push('/tipomaquinas');
    }

    render() {
        const {item} = this.state;
        const title = <h2>{item.idTipo ? 'Edit Client' : 'Add Client'}</h2>;

        return <div>
            <NavigationBar/>
            <Container>
                {title}
                <Form onSubmit={this.handleSubmit}>
                    <FormGroup>
                        <Label for="nombre">Name</Label>
                        <Input type="text" name="nombre" id="nombre" value={item.nombre || ''}
                               onChange={this.handleChange} autoComplete="name"/>
                    </FormGroup>
                    <FormGroup>
                        <Button color="primary" type="submit">Guardar</Button>{' '}
                        <Button color="secondary" tag={Link} to="/tipomaquinas">Cancelar</Button>
                    </FormGroup>
                </Form>
            </Container>
        </div>
    }
};