import React, {Component} from "react";
import {Navbar, Container,Col} from "react-bootstrap";
export default class Footer extends Component{
    render(){
        let year = new Date().getFullYear();
      return(
          <Navbar fixed={"bottom"} bg={"dark"} variant={"dark"}>
              <Container>
                  <Col className={"text-center text-muted"}>
                      {year} - {year + 1}, Todos los derechos reservados.
                  </Col>
              </Container>
          </Navbar>
      );
    };

};