import React, {Component} from "react";
import {ButtonGroup,Button, Card, Table} from "react-bootstrap";
import axios from 'axios';
import {Link} from "react-router-dom";

export default class TipoMaquinaList extends Component{

    constructor(props) {
        super(props);
        this.state = {
            tipoMaquinas: []
        }
    }
    async componentDidMount() {
        const response = await fetch('/tipomaquinas');
        const body = await response.json();
        this.setState({tipoMaquinas: body});
    }

    async remove(id) {
        await fetch(`/tipomaquinas/${id}`, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(() => {
            let updatedTipoMaquinas= [...this.state.tipoMaquinas].filter(i => i.idTipo !== id);
            this.setState({tipoMaquinas: updatedTipoMaquinas});
        });
    }

    render() {
        const {tipoMaquinas} = this.state;
        return(
        <Card>
            <Card.Header>
                Tipos de Máquinas
            </Card.Header>
            <Card.Body>
                <Table striped bordered hover>
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nombre</th>
                        <th>Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    {tipoMaquinas.map(tipo =>
                        <tr key={tipo.idTipo}>
                            <td>
                                {tipo.idTipo}
                            </td>
                            <td>
                                {tipo.nombre}
                            </td>
                            <td>
                                <ButtonGroup>
                                    <Button tag={Link} to={"/tipomaquinas/" + tipo.idTipo}>Editar</Button>
                                    <Button className={"btn btn-danger"} onClick={() =>
                                    this.remove(tipo.idTipo)}>Borrar</Button>
                                </ButtonGroup>
                            </td>
                        </tr>
                    )}
                    </tbody>
                </Table>
            </Card.Body>
        </Card>
        )
    };

};