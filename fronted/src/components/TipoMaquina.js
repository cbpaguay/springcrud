import React, {Component} from "react";
import {Card,Button} from "react-bootstrap";
import {Form as formb} from "react-bootstrap";
import {Formik,Form,Field} from "formik";
import * as Yup from 'yup'
export default class TipoMaquina extends Component{
        TipoMaquinaForm() {
            return(
                <Formik
                    initialValues={{nombre:""}}
                    validationSchema={Yup.object({
                        nombre: Yup.string()
                            .min(3,"Debe ser de al menos 3 caracteres")
                            .max(15, "La longitud máxima es de 15 caracteres")
                            .required("Este campo es requerido")
                    })}
                    onSubmit={(values, actions) => {
                        setTimeout(() => {
                            alert(JSON.stringify(values, null, 2));
                            actions.setSubmitting(false);
                        }, 1000);
                    }}
                >
                <Card>
                    <Card.Header>
                        Tipos de Máquinas
                    </Card.Header>
                    <Form>
                        <Card.Body>
                            <formb.Group>
                                <Field className={"form-control"} name="nombre" type="text" placeholder="Ingrese el tipo de máquina" />
                            </formb.Group>
                        </Card.Body>
                        <Card.Footer>
                            <Button className={"btn btn-success"} variant="primary" type="submit">Guardar</Button>
                        </Card.Footer>
                    </Form>

                </Card>
                </Formik>
            );
        }

    render(){
        return(
            this.TipoMaquinaForm()
        );

    };

};