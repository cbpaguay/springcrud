import React, {Component} from "react";
import {Jumbotron} from "react-bootstrap";

export default class Welcome extends Component{
   render(){
       return(
           <Jumbotron fluid>
                   <h1>Bienvenido</h1>
                   <p>
                       This is a modified jumbotron that occupies the entire horizontal space of
                       its parent.
                   </p>
           </Jumbotron>
       );
   };
};