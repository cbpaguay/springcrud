import logo from './logo.svg';
import './App.css';
import NavigationBar from "./components/NavigationBar";
import Welcome from "./components/Welcome";
import Footer from "./components/Footer";
import TipoMaquina from "./components/TipoMaquina";
import TipoMaquinaList from "./components/TipoMaquinaList";

import {Container, Row, Col} from "react-bootstrap";
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";

function App() {
    const marginTop = {
        marginTop :"25px"
    }
  return (
      <Router>
      <NavigationBar/>
      <Container>
          <Row>
              <Col lg={12} style={marginTop}>
                  <Switch>
                     <Route path={"/"} exact component={Welcome}/>
                      <Route path={"/tipomaquinas"} exact component={TipoMaquinaList}/>
                      <Route path={"/addtipo"} exact component={TipoMaquina}/>
                      <Route path={"/tipomaquinas/:id"} exact component={TipoMaquina}/>
                  </Switch>
              </Col>
          </Row>
      </Container>
        <Footer/>
      </Router>
  );
}

export default App;
